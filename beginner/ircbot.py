#!/usr/bin/env python3


import sys
import socket
import string
import random
import time

HOST = "irc.freenode.org"
PORT = 6667

NICK = "CyboBot"
IDENT = "CyboBot"
REALNAME = "CyboBot"
MASTER = "CHANGETHIS"
channel = "#cybogame"

readbuffer = ""

s=socket.socket( )
s.connect((HOST, PORT))

s.send(bytes("NICK %s\r\n" % NICK, "UTF-8"))
s.send(bytes("USER %s %s bla :%s\r\n" % (IDENT, HOST, REALNAME), "UTF-8"))
s.send(bytes("JOIN #cybogame\r\n", "UTF-8"));
s.send(bytes("PRIVMSG %s :Hello Master\r\n" % MASTER, "UTF-8"))


prefix = ":cy "
game_started = False
game_round = 0

players = dict()
print(players)


#        1 2 3 4 5 6 7 8 9 101112
row = [0,1,2,3,4,1,2,3,4,1,2,3,4]
col = [0,1,1,1,1,2,2,2,2,3,3,3,3]

grid = [\
    [0,0,0,0],\
    [0,1,5,9],\
    [0,2,6,10],\
    [0,3,7,11],\
    [0,4,8,12]]

# horizontal, diagnals
trins = [\
    [1,5,9],\
    [2,6,10],\
    [3,7,11],\
    [4,8,12],\
    [1,6,11],\
    [2,7,12],\
    [3,6,9],\
    [4,7,10],\
    ]

quads = [\
    [1,2,3,4],\
    [5,6,7,8],\
    [9,10,11,12],\
    ]

ubold = "\u0002"
upurple = "\u000306"
ugreen = "\u000303"
ulime = "\u000309"
uformat = ubold + ulime

def rem_rep(rolls):
    ret = []
    for i,v in enumerate(rolls):
        if v not in ret:
            ret += [v]
    
    return ret

# Beginner scoring, counts diagnals
def score_dice2(rolls):
    global quads, trins
    srolls = set(rolls)
    if len(srolls) < 3:
        return 0
    
    if len(srolls) == 4: # Check for quads
        for i,v in enumerate(quads):
            if rolls == v or list(reversed(rolls)) == v:
                return 32
            if set(rolls) == set(v):
                return 16
            
    if True: # Check for trinities
        for i,v in enumerate(trins): # Trinities in-order
            temp = [x for x in rolls if x in v]
            temp = rem_rep(temp)
            if temp == v or list(reversed(temp)) == v:
                return 9
            if set(temp) == set(v):
                return 3

        for i,v in enumerate(quads):
            # temp is the rolls with irrelevant rolls and duplicates removed
            temp = [x for x in rolls if x in v]
            temp = rem_rep(temp)
            if len(temp) == 3:
                if sorted(temp) == temp or temp == sorted(temp,reverse=True):
                    return 9
                else:
                    return 3    
    return 0

#*************************************************************************
class ScoreCard:
    def __init__(self):
        self.quads = []
        self.trinity = []
        self.points=[]
        self.rolls = []
        self.irolls = []
        self.scores = []
        self.current = []
        self.icurrent = []
        self.total = 0
        self.round = 1
        return
#*************************************************************************
    def get_score(self):
        if round ==1:
            return
        return score_dice2(self.irolls[-1])
    
    def get_score_old(self):
        # Using beginner mode rules:
        global row, col, grid
        if round ==1:
            return
        a = self.irolls[-1]

        sa = list(set(a))

        lrow = [0,0,0,0,0]
        lcol = [0,0,0,0,0]


        for i,v in enumerate(sa):
            lrow[row[v]] += 1
            lcol[col[v]] += 1

        print("scoring: ", lrow, lcol)
        if 3 in lrow:
            t = lrow.index(3)
            # check if its in order
            s = set(grid[t])
            temp3 = [x for x in a if x in s]
            if sorted(temp3) == temp3 or sorted(temp3, reverse=True) == temp3:
                return 9
            else:
                return 3
                
        if 4 in lcol:
            t = lcol.index(4)
            # check if its in order
            s = set()
            for i,v in enumerate(grid):
                s += v[t]
            temp3 = [x for x in a if x in s]
            if sorted(temp3) == temp3 or sorted(temp3, reverse=True) == temp3:
                return 32
            else:
                return 16
            
        if 3 in lcol:
            t = lcol.index(3)
            # check if its in order
            s = set()
            for i,v in enumerate(grid):
                s.add(v[t])
            temp3 = [x for x in a if x in s]
            if sorted(temp3) == temp3 or sorted(temp3, reverse=True) == temp3:
                return 9
            else:
                return 3

        return 0
        
 #*************************************************************************   
    def add_roll(self, n):
        self.icurrent += [n]
        self.current += [str(n).zfill(2)]
        if len(self.current) >= 4:
            self.rolls += [self.current]
            self.irolls += [self.icurrent]
            self.scores += [self.get_score()]
            self.current = []
            self.icurrent = []
            self.round += 1
        return    


#*************************************************************************
#*************************************************************************
def send_msg(dest, msg):
    s.send(bytes("PRIVMSG %s :%s \r\n" % (dest, uformat + msg), "UTF-8"))
    return

def roll_dice():
    millis = int(round(time.time() * 1000))
    r = millis %12 + 1
    return r

def show_scorecard(p, ch):
    send_msg(ch, "***** Displaying Scorecard for: " + p + "****")
    send_msg()
    return

def show_grid(ch):
    send_msg(ch, "..1..5...9..")
    send_msg(ch, "..2..6..10..")
    send_msg(ch, "..3..7..11..")
    send_msg(ch, "..4..8..12..")

#*******************************************************************************

# Rules of game:
# 13 rounds per game

# Game board:
#
#  1   5    9
#  2   6   10
#  3   7   11
#  4   8   12

#
# Order is 9 points: eg 1,5,9 or 9,5,1
# 5,9,1 : Trinity: 3 points
# if they get 3 in a column, may choose to go for a quad
# if get fourth number get 16 points, else get only 3

def score_dice(rolls):
    print("score_dice()", rolls)

    lrow = [0,0,0,0,0]
    lcol = [0,0,0,0,0]

    for i,v in enumerate(rolls):
        lrow[row[v]] += 1
        lcol[col[v]] += 1

    print("scoring: ", lrow, lcol)
    
    return


#********************************************************************************

def process_msg(ch, sender, msg):
    global game_round, players

    if (not msg.startswith(prefix)):
        return

    msg.lstrip(prefix)
    #send_msg(sender, msg)

    tokens = msg.split(" ")
    msg2 = ""

    if "cy" in  tokens:
        send_msg(sender, "Responding to: " + msg)

    if "reset" in tokens:
        players[sender] = ScoreCard()
        send_msg(ch, "Reset Score Card for " + sender)

    if "roll" in tokens:
        if sender not in players:
            send_msg(ch, sender + " has not joined the game yet")
        else:
            dice_roll = roll_dice()
            s = players[sender]
            send_msg(ch, sender + " Rolled Die: " + str(dice_roll))
            s.add_roll(dice_roll)
            if (len(s.current) == 0):
                send_msg(ch, sender + " Finished Round " + str(s.round-1) + " " + ": " + str(s.irolls[-1]) + ", Score:" + str(s.scores[-1]))
                send_msg(sender, sender + " Finished Round " + str(s.round-1) + " " + ": " + str(s.irolls[-1]) + ", Score:" + str(s.scores[-1]))
                #send_msg(ch, sender + ": " + str(s.irolls[-1]) + ", Score:" + str(s.scores[-1]))
                #send_msg(sender, sender + " finished round " + str(s.round-1))
                #send_msg(sender, sender + ": " + str(s.irolls[-1]) + ", Score:" + str(s.scores[-1]))
                show_grid(ch)

        #score_dice(idice)
        #scores.trinity += [msg2]
        
    if "total" in tokens:
        if sender not in players:
            send_msg(ch, sender + " has not joined the game yet")
        else:
            s = players[sender]
            #send_msg(ch, sender + "'s Scorecard:")
            #send_msg(ch, "Dice Rolls..............Score")
            #for i,v in enumerate(s.rolls):
                #send_msg(ch, "Round " + str(i+1) + ":" + str(v) + "..." + str(s.scores[i]))
            send_msg(ch, "Total Score for " + sender + ": " + str(sum(s.scores)))

            
    if "join" in tokens:
        msg = sender + " joined beginner mode."
        players[sender] = ScoreCard()
        send_msg(ch, msg)

    if "rules" in tokens:
        send_msg(ch, "Game Rules: Join the game. Roll dice four time for a round. 3 Points for 3 in-a-row, 9 if its in order. 16 points for 4 in-a-row, 32 if its's in order")

    if "grid" in tokens:
        show_grid(ch)

    if "help" in tokens:
        send_msg(ch, "************ Available Commands:  *****************")
        send_msg(ch, "* cy join   : Start game                          *")
        send_msg(ch, "* cy reset  : reset scorecard                     *")
        send_msg(ch, "* cy roll   : roll a 12-sided die                 *")
        send_msg(ch, "* cy total  : print your total score              *")
        #send_msg(ch, "* cy all    : print all scores to private window  *")
        send_msg(ch, "* cy rules  : print the rules of the game         *")
        send_msg(ch, "* cy grid   : display the game board grid         *")
        send_msg(ch, "************ END Commands   ***********************")
        
    return


#*******************************************************************************

random.seed()

msg = "Hello, I am CyBot! Type \"cy help\" to list available commands"
send_msg(channel, msg)

while 1:
    readbuffer = readbuffer+s.recv(1024).decode("UTF-8")
    temp = str.split(readbuffer, "\n")
    readbuffer=temp.pop( )

    for line in temp:
        line = str.rstrip(line)
        line = str.split(line)

        if(line[0] == "PING"):
            s.send(bytes("PONG %s\r\n" % line[1], "UTF-8"))
        if(line[1] == "PRIVMSG"):
            sender = ""
            for char in line[0]:
                if(char == "!"):
                    break
                if(char != ":"):
                    sender += char 
            size = len(line)
            i = 3
            message = ""
            while(i < size): 
                message += line[i] + " "
                i = i + 1
   
            message.lstrip(":")

            process_msg(channel, sender, message)

        for index, i in enumerate(line):
            print(line[index])
